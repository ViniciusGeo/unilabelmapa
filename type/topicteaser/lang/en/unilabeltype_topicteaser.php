<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * unilabel module
 *
 * @package     mod_unilabel
 * @author      Andreas Grabs <info@grabs-edv.de>
 * @copyright   2018 onwards Grabs EDV {@link https://www.grabs-edv.de}
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$string['carousel'] = 'Carousel';
$string['clickaction'] = 'Click action';
$string['coursenotfound'] = 'Course not found';
$string['default_carouselinterval'] = 'Default interval';
$string['default_clickaction'] = 'Default click action';
$string['default_showcoursetitle'] = 'Default show course title';
$string['default_showintro'] = 'Default show unilabel text';
$string['default_presentation'] = 'Default presentation';
$string['grid'] = 'Grid';
$string['nocontent'] = 'No content';
$string['opendialog'] = 'Open dialog';
$string['opencourseurl'] = 'Open course url';
$string['pluginname'] = 'Topic teaser';
$string['pluginname_help'] = 'This content type shows you the descriptions of the visible sections of the defined course. The content of the section is shown as modal dialog. This content type enables you to show content from other courses inside another course or on the frontpage.';
$string['presentation'] = 'Presentation';
$string['showcoursetitle'] = 'Show course title';
$string['showunilabeltext'] = 'Show unilabel text';
